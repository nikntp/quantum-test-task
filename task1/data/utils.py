import yaml
import os
from spacy import displacy

CONFIG_PATH = ""


def load_config(config_name):
    """
    Load yaml congfig specifying model and dataset parametrs.
    """
    with open(os.path.join(CONFIG_PATH, config_name)) as file:
        config = yaml.safe_load(file)

    return config


def display_labels(text: str, bounds: list[tuple[int, int, str]]) -> None:
    """
    Visualization of labels.
    """
    colors = {'MOUNTAIN': 'linear-gradient(90deg, #aa9cfc, #fc9ce7)'}
    options = {'ents': ['MOUNTAIN'], 'colors': colors}
    ex = [{
       'text': text,
       'ents': [{'start': x[0], 'end': x[1], 'label': x[2]} for x in bounds]
    }]
    displacy.render(ex, style='ent', manual=True, options=options)


def read_txt(filename):
    """
    Reading txt file.
    """
    with open(filename) as f:
        lines = f.readlines()
    lines = list(map(lambda x: x.strip(), lines))
    return lines
