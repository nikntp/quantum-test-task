import torch
from torch.utils.data import DataLoader
from transformers import BertTokenizerFast
from sklearn.model_selection import train_test_split
from .utils import load_config
import pandas as pd
from string import punctuation
import os
os.environ["TOKENIZERS_PARALLELISM"] = "false"

UNIQUE_LABELS = ['O', 'B', 'I']
LABEL_MAPPING = {'O': 0, 'B': 1, 'I': 2}
SPECIAL_LABEL = -100
tokenizer = None

try:
    config = load_config("config.yaml")
except FileNotFoundError:
    ...


def align_label(tokenized_input: dict, labels: list[str]):
    """
    Adjust labels to match the BERT tokenized text.

    Args:
        tokenized_input: BERT tokenized text.
        labels: raw labels.
    """
    word_ids = tokenized_input.word_ids()
    tokens = tokenizer.convert_ids_to_tokens(tokenized_input["input_ids"][0])

    label_ids = []
    shift = 0
    prev = None
    for word_idx, token in zip(word_ids, tokens):
        if (prev in ['-', '\'']) or (token.isnumeric() and prev == ','):
            label_ids.append(0)
            shift += 1
            prev = token
            continue

        if word_idx is None:
            label_ids.append(SPECIAL_LABEL)
        elif token in punctuation:
            label_ids.append(0)
            shift += 1
        else:
            label_ids.append(LABEL_MAPPING[labels[word_idx-shift]])
        prev = token
    return label_ids


def tokenize(raw_text: str):
    """
    Preprocess text by aplying tokenization.
    """
    global tokenizer
    if tokenizer is None:
        tokenizer = BertTokenizerFast.from_pretrained('bert-base-cased')

    return tokenizer(raw_text, padding=config['padding'], max_length=config['max_length'],     # noqa
                     truncation=config['truncation'], return_tensors=config["return_tensors"]) # noqa


def ids_to_tokens(input_ids: list) -> list:
    return tokenizer.convert_ids_to_tokens(input_ids)


class DataSequence(torch.utils.data.Dataset):

    def __init__(self, df):
        labels = [label.split() for label in df['labels'].values.tolist()]
        texts = df['text'].values.tolist()
        self.texts = [tokenize(str(text)) for text in texts]
        self.labels = [align_label(i, j) for i, j in zip(self.texts, labels)]

    def __len__(self):
        return len(self.labels)

    def get_batch_data(self, idx):
        return self.texts[idx]

    def get_batch_labels(self, idx):
        return torch.LongTensor(self.labels[idx])

    def __getitem__(self, idx):
        batch_data = self.get_batch_data(idx)
        batch_labels = self.get_batch_labels(idx)

        return batch_data, batch_labels


def create_dataloaders(df: pd.DataFrame):
    """
    Split dataset into train and test samples and convert them
    to torch.utils.data.Dataloader.
    """
    df_train, df_valid = train_test_split(df, test_size=config['test_size'],
                                          random_state=config['random_state'])

    train_dataloader = DataLoader(
        DataSequence(df_train), num_workers=config['num_workers'],
        batch_size=config['batch_size'], shuffle=config['shuffle_train']
    )
    valid_dataloader = DataLoader(
        DataSequence(df_valid), num_workers=config['num_workers'],
        batch_size=config['batch_size'], shuffle=config['shuffle_valid']
    )

    return train_dataloader, valid_dataloader
