import openai
import os
import re
import pandas as pd
from dotenv import load_dotenv

load_dotenv()

OPENAI_API_KEY = os.environ.get('OPENAI_API_KEY')
PROMPT = (
    "Generate 10 random texts about famous mountains, every sentence should have at least one mountain name."
    "Aim for diversity in terms of geography, language, and sentence length and complexity."
    "Indicate boundaries of every mountain name with tags."
    "Please do not include any numbering or double quotes at the beginning of the lines. "
    "Example:\n"
    "<mountain>Mount Kilimanjaro</mountain>, Africa's highest summit, offers a challenging ascent amid Tanzania's stunning landscapes.\n"
    "The jagged peaks of the <mountain>Himalayas</mountain>, including the iconic <mountain>Mount Everest</mountain>, draw adventurers from around the globe.\n"
    "In the heart of Patagonia, the <mountain>Fitz Roy</mountain> massif boasts towering granite peaks, attracting climbers and nature enthusiasts.\n"
)
NUM_OF_CALLS = 100

if not OPENAI_API_KEY:
    raise ValueError('OPENAI_API_KEY environment variable not set')

openai.api_key = os.environ.get('OPENAI_API_KEY')


def generate_text():
    """
    Make single API call.
    """
    response = openai.ChatCompletion.create(
        model='gpt-4-0314',
        messages=[
            {'role': 'user', 'content': PROMPT}
        ],
        temperature=0.7
    )
    return response.choices[0].message.content.strip()


def tokenized_to_labels(s_tokenized: str) -> str:
    """
    Converte response of gpt model that contains <mountain> / </mountain> tags
    to labels by the rule:
        B - start of mountain name.
        I - continuation of mountaion name.
        O - any other word.

    Args:
        s_tokenized: signle text of gpt response.
    """
    words = re.sub(r'[^\w\s<>/]', '', s_tokenized).split()
    labels = ['O'] * len(words)
    token = False
    for i in range(len(labels)):
        if words[i].startswith('<mountain>'):
            token = True

        if not token:
            continue

        if i == 0:
            labels[i] = 'B'
        else:
            labels[i] = 'B' if labels[i-1] == 'O' else 'I'

        if words[i].endswith('</mountain>'):
            token = False
    return ' '.join(labels)


def create_dataset(texts: list[str]) -> None:
    """
    Parse gpt model response to the format required by the NER model to train.

    Args:
        texts: A list of responses from gpt model each containing <mountain> tags.
    """
    raw_texts = []
    labels = []
    for text in texts:
        raw_texts.append(text.replace('<mountain>', '')
                             .replace('</mountain>', ''))
        labels.append(tokenized_to_labels(text))
    data = pd.DataFrame({'text': raw_texts, 'labels': labels})
    data.to_csv('ner.csv', index=False)


if __name__ == '__main__':
    data = []

    try:
        for i in range(NUM_OF_CALLS):
            response = generate_text()
            lines = response.split("\n")
            for line in lines:
                if line != '':
                    data.append(line)
            print(f'Completed API call {i + 1} of {NUM_OF_CALLS}')
    except KeyboardInterrupt:
        print('\nKeyboard interrupt detected. Saving partial data to the CSV file...')

    finally:
        create_dataset(data)
        print('Data saved to csv.')
