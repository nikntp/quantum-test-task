from transformers import BertForTokenClassification
import torch
from data.dataset import UNIQUE_LABELS, SPECIAL_LABEL, create_dataloaders
from data.utils import load_config
from tqdm import tqdm
import pandas as pd
import os
import sys

try:
    config = load_config("config.yaml")
except FileNotFoundError:
    ...


class NER(torch.nn.Module):
    def __init__(self):
        super(NER, self).__init__()
        self.bert = BertForTokenClassification.from_pretrained(
            'bert-base-cased',
            num_labels=len(UNIQUE_LABELS)
        )

    def forward(self, input_id, mask, label):
        output = self.bert(input_ids=input_id, attention_mask=mask,
                           labels=label, return_dict=False)

        return output


def procces_epoch(model, task, data_loader, optimizer, device):
    """
    Train or validate the model for one epoch.
    """
    total_accuracy = 0
    total_loss = 0

    if task == 'train':
        model.train()
    elif task == 'valid':
        model.eval()

    for data, label in tqdm(data_loader):
        label = label.to(device)
        mask = data['attention_mask'].squeeze(1).to(device)
        input_id = data['input_ids'].squeeze(1).to(device)

        optimizer.zero_grad()
        loss, logits = model(input_id, mask, label)

        for i in range(logits.shape[0]):
            logits_clean = logits[i][label[i] != SPECIAL_LABEL]
            label_clean = label[i][label[i] != SPECIAL_LABEL]

            predictions = logits_clean.argmax(dim=1)
            accuracy = (predictions == label_clean).float().mean()
            total_accuracy += accuracy
            total_loss += loss.item()

        if task == 'train':
            loss.backward()
            optimizer.step()

    return total_accuracy, total_loss


def train(model, train_dataloader, valid_dataloader):
    """
    Train the NER model.
    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)
    model = model.to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=config['learning_rate'])

    train_len = len(train_dataloader) * config['batch_size']
    valid_len = len(valid_dataloader) * config['batch_size']

    history = pd.DataFrame({'accuracy': [], 'loss': [], 'valid_accuracy': [], 'valid_loss': []})

    for epoch_num in range(config['epochs']):

        acc_train, loss_train = procces_epoch(
            model, 'train', train_dataloader, optimizer, device
        )

        val_accuracy, val_loss = procces_epoch(
            model, 'valid', valid_dataloader, optimizer, device
        )

        history.loc[len(history)] = [loss_train/train_len, (acc_train/train_len).item(),
                                     val_loss/valid_len, (val_accuracy/valid_len).item()]

        print('Epochs: {} | Loss: {:.3f} | Accuracy: | {:.3f} | Val_Loss: {:.3f} | Val_Accuracy {:.3f}'
              .format(epoch_num+1, loss_train/train_len, acc_train/train_len, val_loss/valid_len, val_accuracy/valid_len)) # noqa

    return history


if __name__ == '__main__':
    dataset_path = sys.argv[1]
    save_model_path = sys.argv[2]

    df = pd.read_csv(dataset_path)

    model = NER()
    train_dataloader, valid_dataloader = create_dataloaders(df)

    history = train(model, train_dataloader, valid_dataloader)
    history.to_csv(save_model_path, index=False)
    torch.save(model, os.path.join(config['weights_folder'], 'model.pth'))
