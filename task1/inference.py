from string import punctuation
import torch
import sys
from data.dataset import tokenize, ids_to_tokens
from data.utils import read_txt, load_config
from train import NER

IDS_TO_LABELS = {0: 'O', 1: 'B', 2: 'I'}
SPEC_TOKENS = ['[CLS]', '[SEP]', '[PAD]']


def logits_to_labels(tokenized: list, tokens: list, logits: list):
    labels = []
    prev_id = None
    prev = None
    for token, word_id, logit in zip(tokens,
                                     tokenized.word_ids(),
                                     logits[0].detach()[0].argmax(axis=1)):

        if token in SPEC_TOKENS or word_id == prev_id or \
           token in punctuation or prev in ['-', '\''] or (token.isnumeric() and prev == ','): # noqa
            continue

        logit = logit.item()
        labels.append(logit)
        prev_id = word_id
        prev = token
    return labels


def predict(model, texts: list[str]):
    res = []
    for text in texts:
        tokenized = tokenize(text)
        tokens = ids_to_tokens(tokenized["input_ids"][0])
        logits = model(tokenized['input_ids'], tokenized['attention_mask'], None)
        labels = [IDS_TO_LABELS[ids] for ids in logits_to_labels(tokenized, tokens, logits)]
        res.append(' '.join(labels))
    return res


if __name__ == '__main__':
    model_path = sys.argv[1]
    data_path = sys.argv[2]

    data = read_txt(data_path)
    model = torch.load(model_path)
    result = predict(model, data)
    print(result)
