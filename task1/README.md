# Natural Language Processing. Named entity recognition

## Project structure

- data
    - dataset.py - prepares data and creats datasets.
    - generator.py - utilizes ght-4 model to create synthetic dataset.
    - utils.py
- notebooks
    - demo.py - Jupyter notebook with demo.
- train.py
- inference.py
- model.pth - model weights.
- config.yaml - config with model and dataset params.
- env.example - specify your OpenAI API key to generate dataset with generator.py.
- report.pdf - project report.

Google drive link to the pretrained model weights and dataset [Link](https://drive.google.com/drive/folders/1Tga7ctPnozfaszBXLhCJvqZa7w9TeF8Z)
